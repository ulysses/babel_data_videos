#crime data-----------------------------

#crime winners by country of origin each year (top 10)-------------------------
library(data.table)
library(dplyr)
library(zoo)
library(fst)

#read data
crime = fread("country_crime_rates/cleaned_crime.csv", header = T)

#filter for top ten crime
crime = crime[winner_ioc %in% crime[,.(count = .N),.(winner_ioc)][order(count, decreasing = T)][1:10]$winner_ioc]
  
#number of winners by country over time
crime_sum = 
  crime[,.(count = .N)
         ,.(tourney_date =  substr(as.Date(tourney_date),1,4),
            country = winner_ioc)][is.na(country) | country == "" 
                                   ,country:='UNKNOWN']

# calculate cuulative sum
setorder(crime_sum, country, tourney_date)
crime_sum[,count:=ave(count, country, FUN=cumsum)]

#spread
crime_spread = dcast(setDT(crime_sum)
                      ,country  ~ tourney_date
                      , value.var = c("count"))

crime_spread
fwrite(crime_spread, "country_crime_rates/crime_rates_by_country.csv", row.names = F)

